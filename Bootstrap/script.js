const goodAnswers = ["male", "female", "unisex", "unisex", "male", "male", "unisex", "female"];

function checkAnswers() {
    let guesses = document.getElementsByClassName("choice");
    let input = [];


    for (let i = 0; i < guesses.length; i++) {
        let options = document.getElementsByName("gender" + i);

        for (let index = 0; index < options.length; index++) {
            let option = options[index];

            if (option.checked) {
                input.push(option.value);
            }
        }
    }
    let wrongAnswers = 0;

    for (let i = 0; i < goodAnswers.length; i++) {
        if (input[i] != goodAnswers[i]) {
            wrongAnswers++;
        }
    }

    window.alert("You have " + (goodAnswers.length - wrongAnswers) + " good answers!");
}

function markOnGender() {
    var x = document.getElementsByClassName('name');
    var i;
    for (i = 0; i < x.length; i++)
        if (x[i].id === "male") {
            x[i].style.backgroundColor = "red";
        } else if (x[i].id === "female") {
            x[i].style.backgroundColor = "blue";
        } else if (x[i].id === "unisex") {
            x[i].style.backgroundColor = "green";

        }
    }