const baseUrl = "https://image.tmdb.org/t/p/w500";

function createList() {
    let list = document.getElementById("list");
    let onOrderedList = document.createElement("ul");
    for(let item of movies.results){
        console.log(item);
        let listItem = document.createElement("li");
        let text = document.createTextNode(item.original_title);
        listItem.appendChild(text);
        listItem.setAttribute('onclick', 'getDetails('+ item.id + ')');
        onOrderedList.appendChild(listItem);
    }
    list.appendChild(onOrderedList);
}


function getDetails(id) {
    for(let item of movies.results) {
        if(id == item.id){
            showDetails(item);
           
        }
    }
    
}
function showDetails(movies) {
    var detail = document.getElementById("detail");
    detail.innerHTML = "";

    let container = document.createElement('div');
    let img = document.createElement('img');
    let title = document.createElement('h1');
    let overview1 = document.createElement('p');
    let release_date = document.createElement('p');
    let votes1 = document.createElement('p');
    let votesAvg1 = document.createElement('p');

    let release = document.createTextNode("ReleaseDate: " + movies.release_date);
    let votes = document.createTextNode("Total votes: " + movies.vote_count);
    let voteAvg = document.createTextNode("Average vote: " + movies.vote_average + "/10");
    let overview = document.createTextNode("Overview: " + movies.overview);


    let backdropImg = document.createElement('div');
    backdropImg.style.backgroundImage = "url(" + baseUrl + movies.backdrop_path + ")";

    release_date.setAttribute('class', 'main')
    backdropImg.setAttribute('id', 'backdropImg' );
    title.setAttribute('id', 'title');
    overview1.setAttribute('id','overview');
    img.setAttribute('src', baseUrl + movies.poster_path);
    votes1.setAttribute('class', 'main');
    votesAvg1.setAttribute('class', 'main');

    title.appendChild(document.createTextNode(movies.original_title));
    overview1.appendChild(overview);
    release_date.appendChild(release);
    votes1.appendChild(votes);
    votesAvg1.appendChild(voteAvg);
   
    

   // backdropImg.appendChild(title);

    container.appendChild(title);
    container.appendChild(backdropImg);
    container.appendChild(img);
    container.appendChild(overview1);
   // container.appendChild(overview);
    container.appendChild(release_date);
    container.appendChild(votes1);
    container.appendChild(votesAvg1);
    

    document.getElementById('detail').appendChild(container);


}
/*function boldText() {
    var target = document.getElementById("TextArea");
    if(target.style.fontWeight = "bolder");
}

/*function printButtonForList() {
    let list = document.getElementById("detail")
    let onOrderedList = document.createElement("ul")
    for(let item of movies.results) {
        var button = document.createElement("button");
        var text = document.createTextNode(item.original_title)
        listItem.appendChild(text);
        onOrderedList.appendChild(button);
    }
    list.appendChild(onOrderedList);
}*/